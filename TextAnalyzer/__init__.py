"""
The package analyzes given test file.

It can count
* total lines
* empty lines
* lines with 'z'
* 'z' characters count
* lines with 'and'
Usage: ``from TextAnalyzer import analyzer``
"""