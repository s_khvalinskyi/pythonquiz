"""
**summary line** Summary.

**description** This module analyzes text.
"""
from re import search
from functools import reduce


def get_path():
    """Read path only."""
    return input("Provide path to the file: ")


def get_lines(file):
    """Get all lines count."""
    return len(file) + 1


def get_empty_lines(file):
    """Get empty lines count."""
    return len(list(filter(lambda x: x == '\n', file))) + 1


def get_matched_lines(file, pattern):
    """Get count of lines with pattern given."""
    reg = rf'{pattern}+'
    return len(list(filter(lambda x: search(reg, x) is not None, file)))


def get_z_count(file):
    """Count ``z`` in given file."""
    z_in_line = [e.count('z') for e in file]
    return reduce(lambda x, y: x + y, z_in_line)


def main():
    """Perform all stuff."""
    path = get_path()
    print(f"File: {path}")
    descriptor = open(path, 'r')
    file = descriptor.readlines()
    print(f'    total lines:        {get_lines(file)}')
    print(f'    empty lines:        {get_empty_lines(file)}')
    print(f'    lines with \'z\':     {get_matched_lines(file, "z")}')
    print(f'    "z" count:          {get_z_count(file)}')
    print(f'    lines with \'and\':   {get_matched_lines(file, "and")}')


if __name__ == "__main__":
    main()
