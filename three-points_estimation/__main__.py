"""This module reads estimations and calculates confidence interval."""
from .task_estimation.task_estimation import TaskEstimation
from collections import namedtuple


def _get_estimations():
    """Prompt estimation of tasks and return array with TaskEstimation objects"""
    print("Provide estimations for a project. If you finished just press ENTER.")
    project = []
    while True:
        answer = input("Type estimations for a task: ").split(" ")
        if len(answer) == 1:
            break
        elif len(answer) != 3:
            print(f"Provide 3 values, only {len(answer)} given...")
        else:
            a, m, b = answer
            project.append(TaskEstimation(int(a), int(m), int(b)))
    return project


def _count_confidence_interval(project):
    """Calculate confidence interval for a project"""
    expected_value, standard_deviation = (0, 0)
    confidence_interval = namedtuple('confidence_interval', ('minimal_value', 'maximal_value'))
    for task in project:
        expected_value += task.get_weighted_average()
        standard_deviation += task.get_standard_deviation()
    return confidence_interval(expected_value - 2 * standard_deviation, expected_value + 2 * standard_deviation)


def main():
    """Call functions and print result"""
    project = _get_estimations()
    if len(project) == 0:
        print('Provide values at least for one project')
        exit(1)
    ci = _count_confidence_interval(project)
    print(f"Project's 95% confidence interval: {ci.minimal_value} ... {ci.maximal_value} points")


if __name__ == "__main__":
    main()
