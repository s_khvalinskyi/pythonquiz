"""
This package implements logic of confidence intervals calculations.

This is an executable module and cannot be used as a module for other programs.
"""