"""
This package contains TaskEstimation class.

Instances of this class have methods for calculation of weighted averages and standard deviations.
Usage: ``from .task_estimation.task_estimation import TaskEstimation``
"""