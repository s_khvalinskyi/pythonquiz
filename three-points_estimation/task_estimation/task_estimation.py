"""Calculate weighted averages and standard deviations"""


class TaskEstimation:

    def __init__(self, a, m, b):
        self.a = a
        self.m = m
        self.b = b

    def get_weighted_average(self):
        return (self.a + 4 * self.m + self.b) / 6

    def get_standard_deviation(self):
        return (self.b - self.a) / 6
