from sys import argv
from itertools import combinations


def find_pairs(collection):
    return list(
        filter(
            lambda x: sum(x) == 10, combinations(
                list(map(lambda x: int(x), collection)), 2
            )
        )
    )


def main(collection):
    if collection and (len(collection) % 2 == 0):
        array_with_pairs = find_pairs(collection)
        for pair in array_with_pairs:
            print("%d + %d" % (pair[0], pair[1]))
    else:
        print("Provide pairs of numbers")
        exit(1)


if __name__ == "__main__":
    main(argv[1:])
