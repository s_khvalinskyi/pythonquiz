"""
This package contains number-to-words converter.

It uses library num2words.
Usage: ``from number_to_words import converter``
"""