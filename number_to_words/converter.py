"""This module converts words to numbers."""
from num2words import num2words


def get_words(number):
    """Take number and return representation in words. Fail if not a number passed."""
    try:
        return num2words(int(number))
    except ValueError:
        print('Only integer numbers are accepted')
        exit(1)
