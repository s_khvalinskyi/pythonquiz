"""This module combines greeter and converter. I know, this description isn't very helpful."""
from greeter import greeter
from number_to_words import converter


def main():
    """Greet user, ask him/her for a number, and translate the number into words."""
    greeter.greeting()
    number = input("Please, enter some number: ")
    print(f"You entered: {converter.get_words(number)}")


if __name__ == '__main__':
    main()
