from silters.Handlers.abstract_handler import AbstractHandler


class AnnotationHandler(AbstractHandler):

    def __evaluate_positive(self, line):
        result = []
        for rule in self.positive_rules_objects:
            if rule.matches(line):
                result.append(rule.name)
        return result

    def __evaluate_negative(self, line):
        result = []
        for rule in self.negative_rules_objects:
            if not rule.matches(line):
                result.append(rule.name)
        return result

    def evaluate(self, line: str):
        rules = self.__evaluate_positive(line)
        rules.extend(self.__evaluate_negative(line))
        return rules
