import silters.Rules.rules as filter_rules
from silters.Rules.rules import *
from silters.Handlers.abstract_handler import AbstractHandler
from re import search


class FilterHandler(AbstractHandler):

    def __evaluate_line(self, line, array):
        """evaluate line against positive or negative rules"""
        if len(array) > 1:
            return array[0].matches(line) | self.__evaluate_line(line, array[1:])
        else:
            return array[0].matches(line)

    def evaluate(self, line):
        """
        get an evaluation for both positive and negative rules
        and get count summary evaluation based on positive rules priority

        BTW positive rules priority makes useless negative rules appliance
        """
        return self.__evaluate_line(line, self.positive_rules_objects) \
               or (
                       self.__evaluate_line(line, self.positive_rules_objects)
                       > self.__evaluate_line(line, self.negative_rules_objects)
               )
