from abc import ABC, abstractmethod
import silters.Rules.rules as filter_rules
from silters.Rules.rules import *


class AbstractHandler(ABC):
    def __init__(self):
        # extract rules class names to the array
        self.positive_rules_string = list(filter(lambda x: search(r'FP\d\d\d', x), dir(filter_rules)))
        self.negative_rules_string = list(filter(lambda x: search(r'FN\d\d\d', x), dir(filter_rules)))
        # convert the string array to the object array
        self.positive_rules_objects = list(map(lambda x: eval(f'{x}()'), self.positive_rules_string))
        self.negative_rules_objects = list(map(lambda x: eval(f'{x}()'), self.negative_rules_string))

    @abstractmethod
    def evaluate(self, line: str):
        """Evaluates a line according to rules"""
        pass
