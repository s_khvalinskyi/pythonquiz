from silters.Handlers.filter_handler import FilterHandler
from silters.Handlers.annotation_handler import AnnotationHandler
from sys import argv


def main():
    parameter, path = argv[1:]
    file = open(path, 'r').readlines()

    if parameter == 'filter':
        f_handler = FilterHandler()
        for line in file:
            if f_handler.evaluate(line):
                print(line)
    elif parameter == 'annotate':
        a_handler = AnnotationHandler()
        for line_index in range(len(file)):
            print(f'{line_index + 1}: {str(a_handler.evaluate(file[line_index])).replace("[", "").replace("]", "").replace(",", "")}')
    else:
        print("wrong parameter")
        exit(1)


if __name__ == '__main__':
    main()
