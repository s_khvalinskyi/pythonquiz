from silters.Rules.filter import Filter
from re import search


class FP001(Filter):
    def __init__(self):
        self.name = 'FP001'

    def matches(self, line):
        """check that line ends with a dot"""
        result = bool(search(r'.*\.$', line))
        return result

    def name(self):
        return self.name


class FP002(Filter):
    def __init__(self):
        self.name = 'FP002'

    def matches(self, line):
        """is less than 100 characters"""
        result = len(line) < 100
        return result

    def name(self):
        return self.name


class FP003(Filter):
    def __init__(self):
        self.name = 'FP003'

    def matches(self, line):
        """has at least 5 'a' letters"""
        result = line.count('a') >= 5
        return result

    def name(self):
        return self.name


class FN201(Filter):
    def __init__(self):
        self.name = 'FN201'

    def matches(self, line):
        """has more than 3 z letters"""
        """as a negative rule it should return false on success"""
        result = not line.count('z') > 3
        return result

    def name(self):
        return self.name


class FN202(Filter):
    def __init__(self):
        self.name = 'FN202'

    def matches(self, line):
        """is an empty line"""
        """as a negative rule it should return false on success"""
        result = not len(line) == 0
        return result

    def name(self):
        return self.name


class FN203(Filter):
    def __init__(self):
        self.name = 'FN203'

    def matches(self, line):
        """consists only from non-letter characters"""
        """as a negative rule it should return false on success"""
        result = not bool(search(r'^\W+$', line))
        return result

    def name(self):
        return self.name
