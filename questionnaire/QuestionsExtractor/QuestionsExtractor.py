from os import listdir, getcwd


class QuestionsExtractor:
    def __init__(self, catalog):
        self.catalog = catalog

    def __iter__(self):
        for file in listdir(self.catalog):
            with open(f'./{self.catalog}/{file}', 'r') as section:
                yield section.readlines()
