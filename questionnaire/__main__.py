from .QuestionsExtractor.QuestionsExtractor import QuestionsExtractor
from time import time


def io_exception_handler(func):
    def decorator(*args, **kwargs):
        try:
            result = func(*args, **kwargs)
        except IOError as err:
            print(f'An IO exception is happened! {err}')
            exit(1)
        finally:
            return result
    return decorator


@io_exception_handler
def get_time(func):
    def wrapper(*args, **kwargs):
        start = time()
        result = func(*args, **kwargs)
        end = time()
        with open('time_stats.txt', 'a+') as time_results:
            if func.__name__ == 'main':
                scope = 'Overall time'
            else:
                scope = args[0].strip()
            time_results.writelines(f'{scope} : {end - start} seconds\n')
        return result
    return wrapper


@get_time
def ask_questions(question):
    return input(f'{question.strip()} : ')


@io_exception_handler
@get_time
def main():
    file = 'results.txt'
    directory = './questionnaire/data_source'
    with open(file, 'a+') as results:
        for section in QuestionsExtractor(directory):
            for question in section:
                results.writelines(f'{question.strip()} : {ask_questions(question)}\n')


if __name__ == '__main__':
    main()
