import logging_example
from logging_example.unrealmath.addition import add
from logging_example.unrealmath.subtraction import reduce
import logging
import sys


def config_logging(logger, handler):
    logger.setLevel(logging.INFO)
    handler.setFormatter(logging.Formatter("%(asctime)s - %(message)s"))
    logger.addHandler(handler)


if __name__ == '__main__':
    # console log configs
    config_logging(
        logging.getLogger(logging_example.unrealmath.__name__),
        logging.StreamHandler(sys.stdout)
    )

    # file logs configs
    config_logging(
        logging.getLogger(logging_example.unrealmath.addition.__name__),
        logging.FileHandler('addition.log')
    )
    config_logging(
        logging.getLogger(logging_example.unrealmath.subtraction.__name__),
        logging.FileHandler('subtraction.log')
    )

    add(4, 6)
    reduce(5, 7)
