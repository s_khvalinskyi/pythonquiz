import random
import logging
import sys

__log = logging.getLogger(__name__)


def add(first, second):
    __log.info("%s() function args: first = %s, second = %s", sys._getframe().f_code.co_name, first, second)
    return first + second - random.randint(1, 10)
