import random
import logging
import sys

__log = logging.getLogger(__name__)


def reduce(minuend, subtrahend):
    __log.info("%s() function args: minuend = %s, subtrahend = %s", sys._getframe().f_code.co_name, minuend, subtrahend)
    return minuend - subtrahend + random.randint(1, 10)
