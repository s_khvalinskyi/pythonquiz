from unittest import TestSuite, TextTestRunner
from unit_testing.tests.test_add import TestAdd
from unit_testing.tests.test_substract import TestSubtract


def tests_loader():
    suite = TestSuite()
    suite.addTest(TestAdd('test_one_argument_is_zero'))
    suite.addTest(TestSubtract('test_one_argument_is_zero'))
    return suite


if __name__ == '__main__':
    runner = TextTestRunner()
    runner.run(tests_loader())
