from unittest import TestCase, main
from unit_testing.calculation import subtract


class TestSubtract(TestCase):
    def test_arguments_greater_than_zero(self):
        x, y = 21, 56
        result = self.__subtract_arguments(x, y)
        self.assertEqual(result, subtract(x, y), f'{x} - {y} should be {result}')

    def test_arguments_less_than_zero(self):
        x, y = -56, -876
        result = self.__subtract_arguments(x, y)
        self.assertEqual(result, subtract(x, y), f'{x} - {y} should be {result}')

    def test_one_argument_is_zero(self):
        x, y = 0, 768
        result = self.__subtract_arguments(x, y)
        self.assertEqual(result, subtract(x, y), f'{x} - {y} should be {result}')

    @staticmethod
    def __subtract_arguments(x, y):
        return x - y


if __name__ == '__main__':
    main()
