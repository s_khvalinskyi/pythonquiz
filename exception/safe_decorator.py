import functools
import time


def timer(func):
    """Prints the runtime of the decorated function."""
    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        def count_time(start):
            end_time = time.perf_counter()
            run_time = end_time - start
            print(f"Finished {func.__name__!r} in {run_time:.4f} secs")
        start_time = time.perf_counter()
        try:
            value = func(*args, **kwargs)
        except Exception as error:
            count_time(start_time)
            raise error
        else:
            count_time(start_time)
            return value
    return wrapper_timer


@timer
def throwing_function():
    raise Exception("I'm an exception!")


if __name__ == '__main__':
    throwing_function()
